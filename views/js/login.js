
//------------modalFade-------------------------------------//

function modalFade() {
   $('#student_login').modal('hide');
   $('#student_signUp').modal('hide');
   $('#teacher_login').modal('hide');
   $('#teacher_signUp').modal('hide');

}
//------------------------ logIn --------------------------//
function logIn() {

   var email = $("#email").val();
   var password = $("#pwd").val();

   var logIn = {
      email: email,
      password: password
   }
   console.log(JSON.stringify(logIn));
   if (email==null || email==""||password==null || password==""){  
    
      return false;  
    }
    else{
   $.post("/user/login",
      logIn,
      function (data, status) {
         console.log(data)
         if (data && data.code.role == 'STUDENT') {
                  console.log(data)
             localStorage.setItem('userId', data.code.userId);      
             localStorage.setItem('name', data.code.name);
             localStorage.setItem('branch', data.code.branchName);
             localStorage.setItem('branch_id', data.code.branchId+ "")
             setTimeout(()=>{
               window.location.href = 'studentView.html';
             },1000)
             //window.location.href = 'studentView.html';
            return
         }
         else if (data && data.code.role == 'TEACHER') {
                  console.log(data);
                  localStorage.setItem('userId', data.code.userId);      
                  localStorage.setItem('name', data.code.name);
                  localStorage.setItem('teacherId', data.code.teacherId + "");
            //alert(status)
            window.location.href = '../instructor/instructor1.html', true;
            return;
         }
         else {
            alert(data.message);
            return;
         }
      });
}
}

//--------- forgot-----------------------------//


function forgotPassword() {

   var email = $("#emailF").val();
   var forgotPassword = {
      email: email,
   }
   console.log(JSON.stringify(forgotPassword));
   if (email == null || email == "") {
      alert("Enter email");
      return false;
   }
   else {
      $.post("/user/forgot",
         forgotPassword,
         function (data) {
            if (data == "email send") {


               $("#emailF").val('');
               $('#forgotPassword').modal('hide');
               alert("email send");
            }
            else {
               alert("THIS EMAIL IS NOT REGISTER");
            }

         });
   }
}


//----reset password----//

function recoverPassword() {
   var newPassword = $("#nPwd").val();
   var otp = $("#otp").val();
   var resetPassword = {
      otp: otp,
      newPassword: newPassword
   }
   console.log(JSON.stringify(resetPassword));
   if (otp == null || otp == "" || newPassword == null || newPassword == "") {
      alert("Enter otp and newPassword");
      return false;
   }
   else {
      $.post("/user/updatePassword",
         resetPassword,
         function (data) {
            if (data == "password updated") {
               alert("password updated")
            }
            else {
               alert("error")
            }
            $("#nPwd").val('');
            $("#otp").val('');
            $('#recoverPassword').modal('hide');

         });
   }
}

