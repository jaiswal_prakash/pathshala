$(document).ready(function () {
  getTopicDetail();
  // Recommended_video();
});

var video_id = "";

function postComment() {
  var topic_id = video_id;
  var user_id = localStorage.getItem("userId");
  var comment = $("#comment").val();

  var postComment = {
    topic_id: topic_id,
    user_id: user_id,
    comment: comment,
  };
  $.post("/topic/postComment", postComment, function (data, status) {
    if (data) {
      console.log("comments", data);

      $("#comment").val("");
      //getTopicDetail()
    } else {
      alert(status);
    }
  });
}

function getTopicDetail() {
  var getTopicDetail = {
    _id: localStorage.getItem("_id"),
  };
  $.post("/topic/getTopicDetail", getTopicDetail, function (data, status) {
    if (data) {
      $("#videoUrl").html(
        `<source src="${data[0].videoUrl}" type="video/mp4">`
      );
      $("#title").html(data[0].title); // span
      $("#UploadedDate").html(data[0].UploadedDate); //span
      $("#documentUrl").attr("href", data[0].documentUrl); // href
      $("#description").append(data[0].description); // paragraph
      $("#like").text(data[0].liked.length);
      $("#dislike").text(data[0].disliked.length);

      localStorage.setItem("subject_id", data[0].subjectId._id + "");
      let view = data[0].viewed;
      $("#view").html(view.length); // span
      video_id = data[0]._id;
      var comment = data[0].comments;
      //comment.reverse()
      comment.map((o) => {
        $("#getComment2").append(`<hr>  <div >
            <img src="./images/user.png" alt="user" style="width:60px;  border-radius: 50%; float:left; margin-right:10px">
            <div class="media-body">
              <h4>${o.id.name}<small><i style="color:red"> Posted on ${o.date}</i></small></h4>
              <p style="text-align: left; color:purple">${o.text}</p>      
            </div>
          </div>
          <hr>`);
      });
      Recommended_video();
    } else {
    }
  });
  // Recommended_video();
}

// this is for recommended video to play
function play(value) {
  var topic_id = value;
  localStorage.setItem("topic_id", topic_id + "");
  var userId = localStorage.getItem("userId");

  var view = {
    topic_id: topic_id,
    userId: userId,
  };
  $.post("/topic/view", view, function (data, status) {
    if (data) {
      localStorage.setItem("_id", topic_id);
      window.location.href = "videoPlayer.html";
    } else {
    }
  });
}
// ---------- shuffle video  array ----------//

function shuffle(array) {
  array.sort(() => Math.random() - 0.5);
}
// ---------- Recommended video ----------//

function Recommended_video() {
  var Recommended = {
    subjectId: localStorage.getItem("subject_id"),
  };
  $.post("/topic/Recommended_video", Recommended, function (data, status) {
    if (data && data.length > 0) {
      shuffle(data);
      data.map((o) => {
        $("#Recommended_video").append(`
                        <div class="col-sm-12" style=" margin-right:0 px;margin-top: 5px; margin-bottom: 10px;" >
                          <img
                          src="${o.imageUrl}"
                            alt="Recomended video "
                            style="width: 300px; height: 200px;"
                            onclick="play('${o._id}')"
                            class="pointer"
                          />
                      <br>
                        <!-- image details -->
                        
                          <span style="font-size: large; color: firebrick; text-align: left;"  >${o.title}
                          </span ><br>
                          <span> ${o.branchId.branchName}, </span> 
                          <span> ${o.semesterId.semesterName},</span>
                          <span> ${o.subjectId.subjectName}</span><br />
                          <span style="font-size:15px; color: brown;">Instructor :-</span> <span style="font-size: 15px; color: black;">${o.teacherId.name} </span><br>
                        </div>
                        </div>
                        `);
      });
    } else {
    }
  });
}

// ----------------- for liking the video-----------------//

function like() {
  var topic_id = localStorage.getItem("topic_id");
  var userId = localStorage.getItem("userId");
  var like = {
    topic_id: topic_id,
    userId: userId,
  };
  $.post("/topic/like", like, function (data, status) {
    if (data) {
      console.log(data);
      $("#like").text(data[0].liked.length);
    } else {
    }
  });
}
// ----------------- for disliking the video-----------------//
function dislike() {
  var topic_id = localStorage.getItem("topic_id");
  var userId = localStorage.getItem("userId");
  var dislike = {
    topic_id: topic_id,
    userId: userId,
  };
  $.post("/topic/dislike", dislike, function (data, status) {
    if (data) {
      $("#dislike").text(data[0].disliked.length);
    } else {
    }
  });
}
function searchVideo() {
  var searchText = $("#searchText").val();
  if (searchText == null || searchText == "") {
    return false;
  } else {
    localStorage.setItem("searchText", searchText);
    window.location.href = "view.html";
  }
}
// ----------------- for wishlist the video-----------------//

function wishList() {
  var userId = localStorage.getItem("userId");
  var topicId = localStorage.getItem("topic_id");
  var wishlist = {
    userId: userId,
    topicId: topicId,
  };
  $.post("/user/wishlist", wishlist, function (data, status) {
    if (data) {
    } else {
    }
  });
}

function getWishList() {
  var userId = localStorage.getItem("userId");
  $.get("user/getWishlist" + userId),
    function (data, status) {
      if (data) {
      } else {
      }
    };
}
