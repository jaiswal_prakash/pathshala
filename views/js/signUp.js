//------------ document.ready function---------------------//

$(document).ready(function () {
  getBranchList();
  getSemesterList();
})
//-----------------studentSignUp-----------------------//
function studentSignUp() {
  $('#student_login').modal('hide');
  var name = $("#nameSS").val();
  var email = $("#emailSS").val();
  var phone = $("#phoneSS").val();
  var password = $("#pwdSS").val();
  var usn = $("#usnSS").val();
  var branch = $('#branchLst').val();
  var signUp = {
    name: name,
    email: email,
    password: password,
    phone: phone,
    usn: usn,
    branch: branch
  }
  if (name == null || name == "" || email == null || email == "" || phone == null || phone == "" || password == null || password == "" || usn == null || usn == "" || branch == null || branch == "") {

    return false;
  }
  else {
    console.log(JSON.stringify(signUp));
    $.post("/student/signup",
      signUp,
      function (data, status) {
        if (data.status == "Success") {
          alert(data.message);
          setTimeout(() => {
            window.location.href = '/#log';
          }, 1000)
        }
        else {
          alert(data.message)
        }
      });
  }
}
//-----------------teacherSignUp-----------------------//

function teacherSignUp() {
  $('#teacher_login').modal('hide');
  var name = $("#nameTS").val();
  var email = $("#emailTS").val();
  var phone = $("#phoneTS").val();
  var password = $("#pwdTS").val();
  var designation = $("#Designation").val();
  var description = $("#description").val();

  var signUp = {
    name: name,
    email: email,
    password: password,
    phone: phone,
    designation: designation,
    description: description 
  }
  if (name == null || name == "" || email == null || email == "" || phone == null || phone == "" || password == null || password == "" || Designation == null || Designation == "") {

    return false;
  }
  else {
    console.log(JSON.stringify(signUp));
    $.post("/teacher/signup",
      signUp,
      function (data, status) {
        if (data.status == "Success") {
          alert(data.message);
          setTimeout(() => {
            window.location.href = '/#log';
          }, 1000)
        }
        else {
          alert(data.message)
        }
      });
  }
}
//--------- get branch list-----------------//
function getBranchList() {
  $.get("/branch/getBranchList",
    function (data, status) {
      console.log(data);
      $('#branchLst').append('<option value= "" hidden >select branch</option>')
      data.map(o => {
        $('#branchLst').append(`<option value= ${o._id} >${o.branchName}</option>`);
      });
    }
  )
};
//--------- get semester list-----------------//
function getSemesterList() {
  $.get("/semester/getSemesterList",
    function (data, status) {
      $('#semesterLst').append('<option value= "" hidden >select semester</option>')
      data.map(o => {
        $('#semesterLst').append(`<option value= ${o._id} >${o.semesterName}</option>`);
      });

    }
  )
}

//--------- get subject list-----------------//







