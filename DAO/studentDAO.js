const studentModel = require('../Model/student');
const UserModel = require('../Model/User');
const userDAO = require('./userDAO');
const constant = require('../utility/constant')
const studentDAO = {
    create: async (bodyData) => {
        try {
            bodyData['role'] = 'STUDENT';
            let userCreated = await userDAO.create(bodyData); // for creating the user collection 
            if (userCreated) {
                console.log('userCreated' + userCreated);
                return new studentModel({
                    name: bodyData.name,
                    studentId: ('STU' + Math.floor(Math.random() * 1000)),
                    branchId: bodyData.branch,
                    usn: bodyData.usn,
                    userId: userCreated._id
                }).save();
            }
        } catch (error) {
            return { status: constant.HTML_STATUS_CODE.INTERNAL_ERROR, message: constant.MESSAGE.COMMON.INTERNAL_ERROR }
        }
    },


    getDetail: async (bodyData) => {
        //console.log(bodyData);
        try {
            let data = await studentModel.find({ userId: bodyData }).
                populate({
                    path: 'branchId',
                    select: { branchName: 1 }
                })
            return data;

        } catch (error) {
            return { status: constant.HTML_STATUS_CODE.INTERNAL_ERROR, message: constant.MESSAGE.COMMON.INTERNAL_ERROR }
        }

    },
}

module.exports = studentDAO;