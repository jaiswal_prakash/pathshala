const UserModel = require('../Model/User');
const studentModel = require('../Model/student');
const teacherModel = require('../Model/teacher');
var nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const constant = require('../utility/constant');


var Transport = nodemailer.createTransport('SMTP', {
    auth: {
        user: "mooc2k20@gmail.com",
        pass: "1ve17cs082"
    }
});
const UserDAO = {

    create: (bodyData, role) => {
        return new UserModel({
            name: bodyData.name,
            role: bodyData.role, // i need to take this give this value automatically
            email: bodyData.email,
            password: bodyData.password,
            contact: bodyData.phone,
            userId: ('US' + Math.floor(Math.random() * 1000)),
            otp: 'otp123',

        }).save();
    },
    login: async function (bodyData) {
        let data = await UserModel.findOne({ email: bodyData.email });
        if (!data) {
            let code = {
                role: " "
            }
            return { message: constant.MESSAGE.LOGIN.EMAIL_NOT_VALID, status: constant.HTML_STATUS_CODE.INVALID_DATA, code: code }
        };

        const isMatch = await bcrypt.compare(bodyData.password, data.password);

        if (!isMatch) {
            let code = {
                role: " "
            }
            return { message: constant.MESSAGE.LOGIN.WRONG_PASSWORD, status: constant.HTML_STATUS_CODE.INVALID_DATA, code: code }
        }
        else {
            if (data && data.role == 'STUDENT') {
                let userId = data._id;
                let studentData = await studentModel.findOne({ userId: userId }).populate({
                    path: 'branchId',
                    select: { branchName: 1, _id: 1 }
                });
                let code = {
                    userId: userId,
                    role: data.role,
                    branchId: studentData.branchId._id,
                    branchName: studentData.branchId.branchName,
                    name: studentData.name
                }
                return { message: constant.MESSAGE.LOGIN.SUCCESS, status: constant.HTML_STATUS_CODE.SUCCESS, code: code };
            }
            if (data && data.role == 'TEACHER') {
                let userId = data._id;
                let teacherData = await teacherModel.findOne({ userId: userId })
                console.log(teacherData);
                let code = {
                    userId: userId,
                    role: data.role,
                    name: teacherData.name,
                    teacherId: teacherData._id
                }
                return { message: constant.MESSAGE.LOGIN.SUCCESS, status: constant.HTML_STATUS_CODE.SUCCESS, code: code };
            }
            else return false;
        }
    },
    // generateAuthToken:async function(bodyData){
    //     const user =bodyData;
    //     const token =jwt.sign({_id:user.userId.toString()},'thisIsNewUser');
    //     console.log(token);

    //     // UserModel.tokens=UserModel.tokens.concat({token:token});
    //     //await UserModel.save();

    //     return token;

    // },

    forgot: async function (bodyData) {
        var data = await UserModel.find({ email: bodyData.email })
        let sendOtp = Math.floor(Math.random() * 1000000); // create otp 

        if (data.length > 0) {
            await UserModel.updateOne({ email: bodyData.email }, { $set: { otp: sendOtp } });
            var mailOptions = {

                from: 'mooc2k20@gmail.com',
                to: bodyData.email,
                subject: 'otp',
                text: sendOtp,
                html: `<br><p style="color:blue;">This is the otp for the forgot password.</p> <br>Please enter the otp  to recover or change password <h1 style="color:red;">${sendOtp}</h1> `
            };
            setTimeout(function () {
                Transport.sendMail(mailOptions, function (error, response) {

                    if (error) {
                        console.log(error);
                        return false;
                    } else {
                        return true;
                    }
                });
            })
            return { message: constant.LOGIN.EMAIL_SUCCESS, status: constant.HTML_STATUS_CODE.SUCCESS };
        }
        else {
            return { message: constant.LOGIN.EMAIL_NOT_VALID, status: constant.HTML_STATUS_CODE.UNAUTHORIZED };
        }
    },
    updatePassword: async function (bodyData) {
        try {
            var data = await UserModel.find({ otp: bodyData.otp })
            if (data.length > 0) {
                
                await UserModel.updateOne({ otp: bodyData.otp }, { $set: { password: bodyData.newPassword } });
                return { message: constant.MESSAGE.LOGIN.PASSWORD_CHANGED, status: constant.HTML_STATUS_CODE.SUCCESS }
            }
            else {
                return { message: constant.MESSAGE.LOGIN.PASSWORD_CHG_FAILED, status: constant.HTML_STATUS_CODE.INVALID_DATA }
            }
        } catch (error) {
            res.status(constant.HTML_STATUS_CODE.INTERNAL_ERROR).send(constant.MESSAGE.COMMON.INTERNAL_ERROR);
        }

    },
    wishList: async (bodyData) => {
        data = await UserModel.updateOne({ _id: bodyData.userId }, { $addToSet: { wishList: bodyData.topicId } });
        if (data) {
            return true;
        }
    },
    getWishList: async (bodyData) => {

    }

}
module.exports = UserDAO;
